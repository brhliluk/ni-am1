package cz.cvut.fit.niam1.hateoas;


import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class RestApplicationRepository {

    private static final List<Tour> tours = new ArrayList<>();
    public ZonedDateTime lastModificationDate = ZonedDateTime.of(LocalDateTime.of(2017, 1, 9,
            10, 30, 20), ZoneId.of("CET"));

    @PostConstruct
    public void initRepo() {
        List<String> customerNames1 = Arrays.asList("Jan", "Petr");
        List<String> customerNames2 = Arrays.asList("Jan", "Petr", "Anicka");
        tours.add(new Tour("cz", "Czech Republic", customerNames1));
        tours.add(new Tour("d", "Germany", customerNames2));
        lastModificationDate = ZonedDateTime.now();
    }

    public void addTour(Tour c) {
        tours.add(c);
        lastModificationDate = ZonedDateTime.now();
    }

    public List<Tour> getTours() {
        return tours;
    }

    public Tour getTourById(String id) {
        return tours.stream().filter(c -> c.getId().equals(id)).findAny().orElse(null);
    }

    public void deleteTour(String id) {
        tours.removeIf(c -> c.getId().equals(id));
        lastModificationDate = ZonedDateTime.now();
    }

}
