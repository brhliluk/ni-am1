package cz.cvut.fit.niam1.hateoas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;


@RestController
@RequestMapping(value = "/tours")
public class RestTourController {

    @Autowired
    RestApplicationRepository repository;

    @GetMapping("/{id}")
    public HttpEntity<Tour> getTour(@PathVariable String id) {
        Tour tour = repository.getTourById(id);
        return ResponseEntity.status(HttpStatus.OK).body(tour);
    }

    @PostMapping(value = "/")
    public ResponseEntity createTour(@RequestBody Tour tour) {
        repository.addTour(tour);
        return ResponseEntity.status(HttpStatus.CREATED).header("Location", "/tour/" + tour.getId()).build();
    }

    @GetMapping("/")
    public ResponseEntity<List<Tour>> getTours(WebRequest request) {
        List<Tour> tours = repository.getTours();
        String currentEtag = String.valueOf(Objects.hash(tours));
        return ResponseEntity
                .status(HttpStatus.OK)
                .eTag(currentEtag)
                .lastModified(repository.lastModificationDate)
                .body(tours);
    }

    @GetMapping("/weak/")
    public ResponseEntity<List<Tour>> getToursWeak(WebRequest request) {
        List<Tour> tours = repository.getTours();
        String currentEtag = String.valueOf(Objects.hash(tours.stream().map(Tour::weakEtag).toArray()));
        return ResponseEntity
                .status(HttpStatus.OK)
                .eTag(String.format("W/\"%s\"", currentEtag))
                .lastModified(repository.lastModificationDate)
                .body(tours);
    }

    @PutMapping("/{id}/customers")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity putCustomers(@PathVariable String id, @RequestBody List<String> customers) {
        repository.getTourById(id).addCustomers(customers);
        repository.lastModificationDate = ZonedDateTime.now();
        return ResponseEntity.status(HttpStatus.CREATED).header("Location", "/tour/" + id).build();
    }

}
