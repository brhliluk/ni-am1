package cz.cvut.fit.niam1.hateoas;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Tour {

    String id;
    String name;
    List<String> customers;

    public Tour(String id, String name, List<String> customers) {
        this.id = id;
        this.name = name;
        this.customers = customers;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCustomers(List<String> customers) {
        this.customers = customers;
    }

    public void addCustomers(List<String> customers) {
        this.customers = Stream.concat(this.customers.stream(), customers.stream())
                .collect(Collectors.toList());
    }

    public List<String> getCustomers() {
        return customers;
    }

    public String weakEtag() {
        return id + name;
    }

}
