= HW7

## ETag
Nejdůležitější část je implementována v souboru `link:/07/src/spring-rest-hateoas-master/main/java/cz/cvut/fit/niam1/hateoas/RestAplication.java[RestAplication]` - 
```
    @Bean
    public ShallowEtagHeaderFilter shallowEtagHeaderFilter() {
        return new ShallowEtagHeaderFilter();
    }
```

Do souboru `link:/07/src/spring-rest-hateoas-master/WEB-INF/web.xml[web.xml]` je také třeba přidat:
```
    <filter>
        <filter-name>etagFilter</filter-name>
        <filter-class>org.springframework.web.filter.ShallowEtagHeaderFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>etagFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
```

Následné použití zahrnuje přidání `.etag("požadovaný hash")` k **ResponseEntity** a posílání získaného ETagu v hlavičce requestů. Hash v obecném případě silného etagu tvořím ze všech získaných `tours`.

`curl -v -X GET "http://localhost:8080/tours/" -H  "accept: \*/*"`

image::07/results/ETag.png[,800]

`curl -v -X GET "http://localhost:8080/tours/" -H  "accept: \*/*" -H  "If-None-Match: \"Vygenerovaný ETag\""`

image::07/results/ETagWithTag.png[,800]

## Weak ETag

Vše je prakticky stejné jako u ETagu, pouze se jako hash využije jen **id** a **name**.
`curl -v -X GET "http://localhost:8080/tours/weak/" -H  "accept: \*/*"`

image::07/results/WeakETag.png[,800]

`curl -v -X GET "http://localhost:8080/tours/weak/" -H  "accept: \*/*" -H  "If-None-Match: W/\"Vygenerované číslo v ETagu\""`

image::07/results/WeakETagWithTag.png[,800]

Na následujícím screenshotu můžete vidět že po přidání nového zákazníka na tour zůstal Weak ETag stejný a silný ETag se změnil.

`curl -X PUT "http://localhost:8080/tours/cz/customers" -H  "accept: \*/*" -H  "Content-Type: application/json" -d "[\"Honza\"]"`

image::07/results/ETagChange.png[,800]

## Last-Modified

Get na `/tours` a `/tours/weak/` můžeme také podmínit tzv **last modified** podmínkou. Úpravy na celém repozitáři mění jeho čas poslední úpravy. Přidává se `.lastModified(repository.lastModificationDate)`

`curl -v -X GET "http://localhost:8080/tours/weak/" -H  "accept: \*/*" -H "If-Modified-Since: Sun, 06 Dec 2020 15:29:30 GMT"`

image::07/results/LastModified.png[,800]
