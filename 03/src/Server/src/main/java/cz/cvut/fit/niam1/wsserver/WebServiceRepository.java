package cz.cvut.fit.niam1.wsserver;

import https.courses_fit_cvut_cz.ni_am1.tutorials.web_services.*;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class WebServiceRepository {

    private static final List<Booking> bookings = new ArrayList<>();

    //Testing, add 2 examples of bookings
    @PostConstruct
    public void initRepo() {
        Booking b1 = new Booking();
        b1.setId(0);
        b1.setFirstName("Joe");
        b1.setLastName("Biden");
        b1.setAirportOfDeparture("Michigan");
        b1.setDateOfDeparture("2020-01-20");
        b1.setAirportOfArrival("Nevada");
        b1.setDateOfArrival("2020-01-20");
        bookings.add(b1);
        Booking b2 = new Booking();
        b2.setId(1);
        b2.setFirstName("Donald");
        b2.setLastName("Trump");
        b2.setAirportOfDeparture("Kentucky");
        b2.setDateOfDeparture("2020-01-20");
        b2.setAirportOfArrival("NewYork");
        b2.setDateOfArrival("2020-01-20");
        bookings.add(b2);
    }

    public void addBooking(Booking b) {
        bookings.add(b);
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void deleteBooking(Integer id) {
        bookings.removeIf(b -> b.getId() == id);
    }

    public void updateBooking(Booking b) {
        deleteBooking(b.getId());
        addBooking(b);
    }


}
