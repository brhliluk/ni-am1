//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.2 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.11.06 at 11:37:48 PM CET 
//


package https.courses_fit_cvut_cz.ni_am1.tutorials.web_services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for booking complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="booking"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="first_name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="last_name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="airport_of_departure" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="date_of_departure" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="airport_of_arrival" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="date_of_arrival" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "booking", propOrder = {
    "id",
    "firstName",
    "lastName",
    "airportOfDeparture",
    "dateOfDeparture",
    "airportOfArrival",
    "dateOfArrival"
})
public class Booking {

    protected int id;
    @XmlElement(name = "first_name", required = true)
    protected String firstName;
    @XmlElement(name = "last_name", required = true)
    protected String lastName;
    @XmlElement(name = "airport_of_departure", required = true)
    protected String airportOfDeparture;
    @XmlElement(name = "date_of_departure", required = true)
    protected String dateOfDeparture;
    @XmlElement(name = "airport_of_arrival", required = true)
    protected String airportOfArrival;
    @XmlElement(name = "date_of_arrival", required = true)
    protected String dateOfArrival;

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the airportOfDeparture property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirportOfDeparture() {
        return airportOfDeparture;
    }

    /**
     * Sets the value of the airportOfDeparture property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirportOfDeparture(String value) {
        this.airportOfDeparture = value;
    }

    /**
     * Gets the value of the dateOfDeparture property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateOfDeparture() {
        return dateOfDeparture;
    }

    /**
     * Sets the value of the dateOfDeparture property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateOfDeparture(String value) {
        this.dateOfDeparture = value;
    }

    /**
     * Gets the value of the airportOfArrival property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirportOfArrival() {
        return airportOfArrival;
    }

    /**
     * Sets the value of the airportOfArrival property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirportOfArrival(String value) {
        this.airportOfArrival = value;
    }

    /**
     * Gets the value of the dateOfArrival property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateOfArrival() {
        return dateOfArrival;
    }

    /**
     * Sets the value of the dateOfArrival property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateOfArrival(String value) {
        this.dateOfArrival = value;
    }

}
