= HW2

Zadání: https://courses.fit.cvut.cz/NI-AM1/hw/02/index.html

Logy se nachází ve složce `link:/02/results[results]`. Jsou to výpisy terminálu po zadání příkazů. Pro přehlednost jsou navíc příkazy vypsány do textových souborů ve složce `link:/02/src[src]`.