import java.io.File

fun main(args: Array<String>) {
    var tourid = ""
    var location = ""
    var name = ""
    val surname: String
    val filelines: List<String>

    // No arguments given
    if (args.isEmpty()) {
        println("Pass file name as an argument!")
        return
    }

    // Try opening file given from args, read line by line
    try {
        filelines = File(args[0]).useLines { it.toList() }
    } catch (e: Exception) {
        println("Input file not found!")
        return
    }
    var list: Array<String> = arrayOf()
    // Parse lines that include quotes into Array of strings
    for (line in filelines) {
        if (line.contains("\"", ignoreCase = true)) {
            list += (line.split("\"").toTypedArray())
        }
    }

    // Find required fields, save their values
    for (i in list.indices) {
        if (list[i] == "Tour id: ") {
            try {
                tourid = list[i + 1]
            } catch (e: Exception) {
                println("Error in file format")
                return
            }
        } else if (list[i] == "Location: ") {
            try {
                location = list[i + 1]
            } catch (e: Exception) {
                println("Error in file format")
                return
            }
        } else if (list[i] == "Person: ") {
            try {
                name = list[i + 1]
            } catch (e: Exception) {
                println("Error in file format")
                return
            }
            break
        }
    }

    if (name.split(" ").size != 2) {
        println("Only 1 name and 1 surname allowed")
        return
    }
    // Split name and surname
    surname = name.split(" ")[1]
    name = name.split(" ")[0]

    // Print in desired format
    println(
        "{\n" +
        "\t\"id\": \"$tourid\"\n" +
        "\t\"location\": \"$location\"\n" +
        "\t\"person\": {\n" +
        "\t\t\"name\": \"$name\"\n" +
        "\t\t\"surname\": \"$surname\"\n" +
        "\t}\n" +
        "}"
    )
}