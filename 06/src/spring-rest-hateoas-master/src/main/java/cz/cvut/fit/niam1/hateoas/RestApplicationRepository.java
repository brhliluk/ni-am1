package cz.cvut.fit.niam1.hateoas;


import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class RestApplicationRepository {

    private static final List<Tour> tours = new ArrayList<>();


    @PostConstruct
    public void initRepo() {
        tours.add(new Tour("cz", "Czech Republic"));
        tours.add(new Tour("d", "Germany"));
    }

    public void addTour(Tour c) {
        int max = tours.stream().map(t-> Integer.valueOf(t.getId())).max(Integer::compare).get();
        c.setId(String.valueOf(max+1));
        tours.add(c);
    }

    public List<Tour> getTours() {
        return tours;
    }

    public Tour getTourById(String id) {
        return tours.stream().filter(c -> c.getId().equals(id)).findAny().orElse(null);
    }

    public void deleteTour(String id) {
        tours.removeIf(c -> c.getId().equals(id));
    }



}
