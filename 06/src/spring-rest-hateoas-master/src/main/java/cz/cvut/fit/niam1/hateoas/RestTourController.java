package cz.cvut.fit.niam1.hateoas;

import cz.cvut.fit.niam1.hateoas.custom.CustomLink;
import cz.cvut.fit.niam1.hateoas.custom.TourLinks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;


@RestController
@RequestMapping(value = "/tour")
public class RestTourController {

    @Autowired
    RestApplicationRepository repository;

    @GetMapping("/{id}")
    public HttpEntity<Tour> getTour(@PathVariable String id) {
        Tour tour = repository.getTourById(id);
        tour.removeLinks();
        tour.add(new Link("http://127.0.0.1:8080/tour/" + id, "self"));
        tour.add(linkTo(RestTourController.class).slash(id).withSelfRel());
        tour.add(linkTo(methodOn(RestTourController.class).getTour(id)).withSelfRel());
        return ResponseEntity.status(HttpStatus.OK).body(tour);
    }

    @PostMapping(value = "/")
    public ResponseEntity createTour(@RequestBody Tour tour) {
        repository.addTour(tour);
        return ResponseEntity.status(HttpStatus.CREATED).header("Location", "/tour/" + tour.getId()).build();
    }

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public List<Tour> getTours(){
        return repository.getTours();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteTour(@PathVariable String id){
        Tour tour = repository.getTourById(id);
        tour.setWillBeDeleted(true);
        CustomLink[] links = new CustomLink[] {new CustomLink("http://127.0.0.1:8080/tour/"+id, "self")};
        TourLinks tl = new TourLinks(tour, links);
        //Wait 10s after that, delete tour
        new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(10);
                repository.deleteTour(id);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        return ResponseEntity.status(HttpStatus.OK).body(tl);
    }

}
