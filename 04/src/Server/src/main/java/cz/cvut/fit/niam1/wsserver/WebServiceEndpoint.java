package cz.cvut.fit.niam1.wsserver;

import https.courses_fit_cvut_cz.ni_am1.tutorials.web_services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.io.IOException;

@Endpoint
public class WebServiceEndpoint {

    @Autowired
    private WebServiceRepository repository;

    //Returns all bookings saved in repository
    @PayloadRoot(namespace = "https://courses.fit.cvut.cz/NI-AM1/tutorials/web-services/", localPart = "getAllOrdersRequest")
    @ResponsePayload
    public GetAllOrdersResponse getBookings(@RequestPayload GetAllOrdersRequest request) {
        GetAllOrdersResponse response = new GetAllOrdersResponse();
        response.getOrder().addAll(repository.getOrders());
        return response;
    }

    //Adds booking into repository, return empty request
    @PayloadRoot(namespace = "https://courses.fit.cvut.cz/NI-AM1/tutorials/web-services/", localPart = "addOrderRequest")
    @ResponsePayload
    public AddOrderResponse addBooking(@RequestPayload AddOrderRequest request) throws IOException {
        AddOrderResponse response = new AddOrderResponse();
        repository.addOrder(request.getOrder());
        return response;
    }

}
