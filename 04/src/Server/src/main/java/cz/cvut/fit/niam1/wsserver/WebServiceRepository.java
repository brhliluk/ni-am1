package cz.cvut.fit.niam1.wsserver;

import https.courses_fit_cvut_cz.ni_am1.tutorials.web_services.Card;
import https.courses_fit_cvut_cz.ni_am1.tutorials.web_services.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.xml.soap.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class WebServiceRepository {

    private static final List<Order> orders = new ArrayList<>();

    //Testing, add 2 examples of bookings
    @PostConstruct
    public void initRepo() {
        Order o1 = new Order();
        Card c1 = new Card();
        o1.setOrderId(0);
        c1.setCardNumber("1111-1111-1111-1111");
        c1.setCardOwner("CardOwner");
        o1.setCard(c1);
        orders.add(o1);
        Order o2 = new Order();
        Card c2 = new Card();
        o2.setOrderId(1);
        c2.setCardNumber("1234-1134-1114-1111");
        c2.setCardOwner("CardOwner2");
        o2.setCard(c2);
        orders.add(o2);
    }

    public void addOrder(Order o) throws IOException {
        if (validateCard(o.getCard()))
            orders.add(o);
    }

    public List<Order> getOrders() {
        return orders;
    }

    // Function that returns true if card is valid
    public Boolean validateCard(Card c) throws IOException {
        createSoapRequest(c);
        // curl POST request from "request.xml"
        String command = "curl  -X POST -H \"Content-Type: text/xml; charset=utf-8\" -d @request.xml http://147.32.233.18:8888/NI-AM1-CardValidation/ws";
        Process process = Runtime.getRuntime().exec(command);
        // Read returned value from curl POST
        BufferedReader in = new BufferedReader(new InputStreamReader(
                process.getInputStream()));
        try {
            // create SOAPMessage from response
            SOAPMessage response = MessageFactory.newInstance().createMessage(null, process.getInputStream());
            if (evaluateCurlResponse(response)) {
                in.close();
                return true;
            }
        } catch (SOAPException e) {
            System.out.println("Printing error");
        }
        in.close();
        return false;
    }

    // Create soap request based upon given card, write to "request.xml"
    public void createSoapRequest(Card c) {
        try {
            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();
            SOAPPart soapPart = soapMessage.getSOAPPart();

            // SOAP Envelope
            SOAPEnvelope envelope = soapPart.getEnvelope();
            envelope.addNamespaceDeclaration("web", "https://courses.fit.cvut.cz/NI-AM1/hw/web-services/");

            // SOAP Body
            SOAPBody soapBody = envelope.getBody();
            SOAPElement soapBodyElem0 = soapBody.addChildElement("validateCardRequest", "web");
            SOAPElement soapBodyElem = soapBodyElem0.addChildElement("cardNumber", "web");
            soapBodyElem.addTextNode(c.getCardNumber());
            SOAPElement soapBodyElem1 = soapBodyElem0.addChildElement("cardOwner", "web");
            soapBodyElem1.addTextNode(c.getCardOwner());

            soapMessage.saveChanges();

            File file = new File("request.xml");
            soapMessage.writeTo(new FileOutputStream(file));
        } catch (SOAPException | IOException e) {
            System.out.println(e);
        }

    }

    // Find element "result" and get its value, return true if result == true
    public Boolean evaluateCurlResponse(SOAPMessage response) {

        Iterator<SOAPBodyElement> it = null;
        try {
            it = response.getSOAPBody().getChildElements();
        } catch (SOAPException e) {
            e.printStackTrace();
        }
        while (it.hasNext()) {
            SOAPBodyElement childNode = it.next();

            if (childNode.getNodeName().contains("validateCardResponse")) {
                Iterator<SOAPBodyElement> params = childNode.getChildElements();

                while (params.hasNext()) {
                    SOAPBodyElement param = params.next();

                    if (param.getNodeName().contains("result")) {
                        return param.getValue().equals("true");
                    }
                }
            }
        }
        return false;
    }

}
