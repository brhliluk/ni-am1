package cz.cvut.fit.niam1.wsclient;

import https.courses_fit_cvut_cz.ni_am1.tutorials.web_services.*;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class WebServiceClient extends WebServiceGatewaySupport {

    public GetAllOrdersResponse getAllOrdersResponse() {

        GetAllOrdersRequest request = new GetAllOrdersRequest();
        return  (GetAllOrdersResponse) getWebServiceTemplate()
                .marshalSendAndReceive(request);
    }

    public AddOrderResponse addOrder(Order o) {

        AddOrderRequest request = new AddOrderRequest();
        request.setOrder(o);
        return  (AddOrderResponse) getWebServiceTemplate()
                .marshalSendAndReceive(request);
    }

}
