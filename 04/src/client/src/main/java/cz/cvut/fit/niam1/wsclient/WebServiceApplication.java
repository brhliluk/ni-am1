package cz.cvut.fit.niam1.wsclient;

import https.courses_fit_cvut_cz.ni_am1.tutorials.web_services.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.stream.Collectors;

@SpringBootApplication
public class WebServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebServiceApplication.class, args);
    }

    @Bean
    CommandLineRunner lookup(WebServiceClient wsClient) {
        return args -> {
            GetAllOrdersResponse response = wsClient.getAllOrdersResponse();
            System.out.println(response.getOrder().stream().map(order -> order.getCard().getCardOwner() + "(" + order.getCard().getCardNumber() + ")").collect(Collectors.joining("; ")));

            Order o = new Order();
            Card c = new Card();
            o.setOrderId(2);
            c.setCardNumber("1234-1234-1234-1234");
            c.setCardOwner("CardOwner");
            o.setCard(c);
            wsClient.addOrder(o);

            Order o1 = new Order();
            Card c1 = new Card();
            o1.setOrderId(3);
            c1.setCardNumber("4444-4444-4444-4444");
            c1.setCardOwner("CardOwner");
            o1.setCard(c1);
            wsClient.addOrder(o1);

            GetAllOrdersResponse response2 = wsClient.getAllOrdersResponse();
            System.out.println(response2.getOrder().stream().map(order -> order.getCard().getCardOwner() + "(" + order.getCard().getCardNumber() + ")").collect(Collectors.joining("; ")));

        };
    }
}