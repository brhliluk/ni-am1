= HW4

Server xsd config (Payment = Order) - `link:/04/results/orders.xsd[Orders]`, Client xml config pro automatické generování - `link:/04/src/client/pom.xml[Pom.xml]`, Kód k serveru je ve složce `link:/04/src/Server[Server]`, klienta najdeme ve složce `link:/04/src/client[client]` oba byly tvořeny na základě template ze stránek předmětu.


Client přidá platbu (order) serveru, ten ji přijme jen po ověření karty skrze další server. Request na ověření karty provádí server za pomocí třídy `SOAPmessage`, která request vytvoří a následně uloží do souboru `request.xml`. Tento soubor je potom předán `curl` příkazu jako POST request na zadanou service. Odpověď je potom opět parsována s pomocí `SOAPmessage` která nejdříve převede zprávu z datového typu string a následně najde element s názvem "result" ze kterého zjistí pozitivní či negativní odpověď.